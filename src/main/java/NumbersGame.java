import java.util.Scanner;

public class NumbersGame {
    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        System.out.println("Hello, what is your name? ");
        String name = sc.nextLine();
        int r = (int)(Math.random()*(100+1))+1;

        System.out.println("Let the game begin!,"+name);
        boolean b = true;
        while (b){

            System.out.println("Enter the number.");
            int k = sc.nextInt();
            if (r<k){
                System.out.println("Your number is too big. Please, try again.");
            } else if (r>k){
                System.out.println("Your number is too small. Please, try again.");
            } else if (r ==  k){
                System.out.println("Congratulations, "+name);
                r = (int)(Math.random()*(100+1))+1;
            }
        }
    }
}
